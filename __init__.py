# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import wage_type
from . import payroll
from . import position
from . import configuration
from . import liquidation
from . import employee
from . import uvt
from . import event_category
from . import account
from . import payroll_auditory
from . import loan
from . import ir


def register():
    Pool.register(
        employee.Employee,
        employee.MandatoryWage,
        position.Position,
        wage_type.WageType,
        payroll.Payroll,
        payroll.PayrollLine,
        configuration.Configuration,
        uvt.UvtWithholding,
        payroll.PayrollGlobalStart,
        payroll.PayrollPaymentStart,
        payroll.PayrollPaycheckStart,
        payroll.PayrollSheetStart,
        liquidation.Liquidation,
        liquidation.LiquidationLine,
        liquidation.LiquidationLineMoveLine,
        liquidation.LiquidationLineAdjustment,
        liquidation.LiquidationAdjustmentStart,
        liquidation.LiquidationGroupStart,
        payroll.PayrollGroupStart,
        event_category.EventCategory,
        payroll.OpenPayrollByPeriodStart,
        payroll.PayrollByPeriodDynamic,
        account.SeverancePayClearingStart,
        account.SeverancePayClearingDone,
        account.Move,
        payroll.Exo2276Start,
        payroll.IncomeWithholdingsStart,
        payroll.PayrollExportStart,
        liquidation.MoveProvisionBonusServiceStart,
        payroll.PayrollSupportDispersionStart,
        employee.UpdateEmployeeStart,
        payroll.PayrollLineMoveLine,
        payroll.PayrollsMultiPaymentStart,
        liquidation.LiquidationExportStart,
        # event.Event,
        loan.LoanLine,
        payroll.PayrollTask,
        ir.Cron,
        module='staff_payroll_co', type_='model')
    Pool.register(
        payroll.PayrollGlobalReport,
        payroll.PayrollPaymentReport,
        payroll.PayrollPaycheckReport,
        payroll.PayrollSheetReport,
        liquidation.LiquidationReport,
        payroll.Exo2276Report,
        payroll.IncomeWithholdingsReport,
        payroll.ExportMovesReport,
        payroll.PayrollExportReport,
        liquidation.LiquidationExportReport,
        wage_type.WageTypeReport,
        payroll_auditory.PayrollAuditoryReport,
        module='staff_payroll_co', type_='report')
    Pool.register(
        payroll.PayrollGlobal,
        payroll.PayrollPayment,
        payroll.PayrollPaycheck,
        payroll.PayrollSheet,
        payroll.PayrollGroup,
        payroll.OpenPayrollByPeriod,
        account.SeverancePayClearing,
        payroll.Exo2276,
        payroll.IncomeWithholdings,
        payroll.PayrollExport,
        liquidation.LiquidationAdjustment,
        liquidation.LiquidationGroup,
        liquidation.MoveProvisionBonusService,
        payroll.PayrollSupportDispersion,
        employee.UpdateEmployee,
        payroll.PayrollsMultiPayment,
        liquidation.LiquidationExport,
        liquidation.LiquidationDeduction,
        payroll_auditory.PayrollAuditory,
        module='staff_payroll_co', type_='wizard')
