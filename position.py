# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Position(metaclass=PoolMeta):
    __name__ = 'staff.position'
    adjust_days_worked = fields.Boolean('Adjust Days Worked',
        help='If is true, rounded month work days on payroll to 30 indeed'
            ' if month has 31 days, or rounded to 15 indeed the biweekly'
            ' pay has 16 days'
        )
    partial_sunday = fields.Boolean('Partial Sunday',
        help='If is true, sunday day is pay for partial amount if start '
            'or end contract in its period')

    @classmethod
    def __setup__(cls):
        super(Position, cls).__setup__()

    def default_adjust_days_worked():
        return True
