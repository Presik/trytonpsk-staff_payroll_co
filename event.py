# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Event(metaclass=PoolMeta):
    'Staff Event'
    __name__ = 'staff.event'
