# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


STATES_PAYROLL = {'invisible': ~Eval('payroll_effect')}


class EventCategory(metaclass=PoolMeta):
    __name__ = 'staff.event_category'
    payroll_effect = fields.Boolean('Payroll Effect')
    discount_method = fields.Selection([
        ('', ''),
        ('uom', 'UoM'),
        ('fixed_amount', 'Fixed Amount'),
        ], 'Discount Method', select=True, states=STATES_PAYROLL)
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type',
        states=STATES_PAYROLL)
    discount_pay = fields.Boolean('Discount Pay', states=STATES_PAYROLL)
    wage_type_discount = fields.Many2One('staff.wage_type',
        'Wage Type Discount', states=STATES_PAYROLL)

    @classmethod
    def __setup__(cls):
        super(EventCategory, cls).__setup__()
