# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.i18n import gettext
from .exceptions import ImportDataEmployeeError


SEX = {
    '': '',
    'hombre': 'male',
    'mujer': 'female'
}

MARITAL = {
    '': '',
    'VIUDO(A)': 'widow',
    'CASADO(A)': 'married',
    'SOLTERO(A)': 'single',
    'UNION_LIBRE': 'free_union',
    'DIVORCIADO(A)': 'divorced'
}

BLOOD = {
    '': '',
    'O+': 'rh_op',
    'O-': 'rh_on',
    'A+': 'rh_ap',
    'A-': 'rh_an',
    'B+': 'rh_bp',
    'B-': 'rh_bn',
    'AB+': 'rh_abp',
    'AB-': 'rh_abn'
}

KIND_CONT = {
    '': '',
    'fijo': 'indefinite',
    'obra_o_labor': 'job',
    'termino_indefinido': 'steady'
}


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    party_health = fields.Function(fields.Many2One('party.party', 'Party Health'),
            'get_party_concept')
    party_retirement = fields.Function(fields.Many2One('party.party', 'Party Retirement'),
            'get_party_concept')
    party_risk = fields.Function(fields.Many2One('party.party', 'Party Risk'),
            'get_party_concept')
    party_box_family = fields.Function(fields.Many2One('party.party', 'Party Box Family'),
            'get_party_concept')
    party_bank = fields.Function(fields.Many2One('party.party', 'Party Bank'),
            'get_party_bank')
    fvp_ind = fields.Numeric('Voluntary Pension Fund Individual', digits=(16, 2),
            help='Voluntary Pension Fund Individual Savings Contribution Art. 55')
    fvp = fields.Numeric('Voluntary Pension Fund', digits=(16, 2),
            help='Voluntary Pension Fund Contribution (FVP) Art. 126')
    afc = fields.Numeric('Savings For the Promotion of Construction', digits=(16, 2),
            help='Savings for the promotion of construction (AFC)')
    housing_interest = fields.Numeric('Housing Interest', digits=(16, 2),
            help='Housing Interest (average for the previous year or corresponding months)')
    health_prepaid = fields.Numeric('Prepaid Medicine', digits=(16, 2),
            help='Health Payments Prepaid Medicine')
    dependents = fields.Numeric('Dependents', digits=(16, 2),
            help='''The taxpayer's children up to 18 years of age or education 18 - 23 years of age.
            Economic dependency spouse, parents, siblings or children supported.''')
    other_income = fields.Numeric('Other Income', digits=(16, 2),
            help='Other Income')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

    def get_party_concept(self, name=None):
        res = None
        name = name[6:]
        for mw in self.mandatory_wages:
            if mw.party and mw.wage_type.type_concept == name:
                res = mw.party.id
                break
        return res

    def get_party_bank(self, name=None):
        return None

    def update_employee(data):
        pool = Pool()
        MandatoryWage = pool.get('staff.payroll.mandatory_wage')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        data = data[1:]
        for row in data:
            emp = str(row[0])
            employees = Employee.search([
                ('party.id_number', '=', emp)
            ])
            if not employees:
                raise ImportDataEmployeeError(
                    gettext('staff_payroll_co.msg_party_not_exists', s=emp))
            else:
                employee = employees[0]

            lines = MandatoryWage.search([
                ('employee', '=', employee.id),
                ('wage_type.type_concept', 'in', [
                    'health', 'retirement', 'risk', 'box_family'
                ])
            ])
            for line in lines:
                concept = line.wage_type.type_concept
                value = None
                if concept == 'health':
                    value = row[1]
                elif concept == 'retirement':
                    value = row[2]
                elif concept == 'risk':
                    value = row[3]
                elif concept == 'box_family':
                    value = row[4]

                if not value:
                    continue
                parties = Party.search([('id_number', '=', value)])
                if parties:
                    MandatoryWage.write([line], {'party': parties[0].id})
                else:
                    raise ImportDataEmployeeError(
                        gettext('staff_payroll_co.msg_party_not_exists', s=value))


class MandatoryWage(metaclass=PoolMeta):
    __name__ = 'staff.payroll.mandatory_wage'

    @classmethod
    def __register__(cls, module_name):
        super(MandatoryWage, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        string_query = "SELECT EXISTS(" \
            " SELECT column_name FROM information_schema.columns" \
            " WHERE table_schema='public'" \
            " and table_name='staff_payroll_mandatory_wage'" \
            " and column_name='defect_amount')"
        cursor.execute(string_query)
        res = cursor.fetchall()
        if res and res[0][0]:
            # try:
            columns=[sql_table.fix_amount],
            cursor.execute(*sql_table.update(
                values=[sql_table.defect_amount]))
            cursor.execute('ALTER TABLE staff_payroll_mandatory_wage DROP COLUMN IF EXISTS defect_amount')
        # cursor.execute('UPDATE staff_payroll_mandatory_wage set fix_amount=defect_amount')
        # except:
        #     print('# WARNING: The column defect_amount not exists')


class UpdateEmployeeStart(ModelView):
    'Update Employee Start'
    __name__ = 'staff.employee.update_employee.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    file = fields.Binary(
        'CSV file', help="Load file csv separated comma in utf-8 format",
        required=True, filename='employees_update')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class UpdateEmployee(Wizard):
    'Update Employee'
    __name__ = 'staff.employee.update_employee'
    start = StateView('staff.employee.update_employee.start',
        'staff_payroll_co.assistant_update_employee_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Update', 'open_', 'tryton-ok', default=True),
        ])

    open_ = StateTransition()

    def transition_open_(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        csv_file = self.start.file.decode('utf-8')
        reader = csv_file.splitlines()
        lista = []
        for r in reader:
            res = r.split(',')
            lista.append(res)
        Employee.update_employee(lista)
        return 'end'
