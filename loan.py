from trytond.pool import PoolMeta


class LoanLine(metaclass=PoolMeta):
    'Loan Line'
    __name__ = 'staff.loan.line'

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return super(LoanLine, cls)._get_origin() + [
                'staff.payroll.line', 'staff.liquidation.line']
