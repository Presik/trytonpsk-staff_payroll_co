# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool

UVT_SUM_FORMULA = {
    28: 10,
    33: 69,
    35: 162,
    37: 268,
    39: 770
}


class UvtWithholding(ModelSQL, ModelView):
    "UVT Withholding"
    __name__ = 'staff.payroll.uvt_withholding'
    start_range = fields.Numeric('Start Range', digits=(16, 0), required=True)
    end_range = fields.Numeric('End Range', digits=(16, 0))
    rate = fields.Float('Rate', digits=(2, 0), required=True)

    @classmethod
    def __setup__(cls):
        super(UvtWithholding, cls).__setup__()

    # @classmethod
    # def __register__(cls, module_name):
    #     table = cls.__table_handler__(module_name)
    #     cursor = Transaction().connection.cursor()
    #
    #     # Migration: Remove old column
    #     uvt_salary_exist = table.column_exist('uvt_salary')
    #     if not uvt_salary_exist:
    #         return
    #
    #     cursor.execute('ALTER TABLE staff_payroll_uvt_withholding DROP COLUMN uvt_salary')

    @classmethod
    def compute_withholding(cls, payment):
        """
        Compute unit price for application of withholdings tax to employee
        """
        res = Decimal('0.0')
        Configuration = Pool().get('staff.configuration')
        configuration = Configuration(1)
        if not configuration.uvt_value:
            return res
        uvt_config = Decimal(configuration.uvt_value)

        # percent_exent = Decimal('0.75')
        # payment2uvt = Decimal(payment * percent_exent) / uvt_config
        payment2uvt = Decimal(payment) / uvt_config
        # print('depurada', Decimal(payment * percent_exent))
        uvt_withholdings = cls.search([
            ['AND', ['OR', [
                ('start_range', '<', payment2uvt),
                ('end_range', '>=', payment2uvt)
            ], [
                ('start_range', '<', payment2uvt),
                ('end_range', '=', None)
            ]]]
        ])
        print('base uvt----->', payment2uvt)
        if uvt_withholdings:
            uvt_wh = uvt_withholdings[0]
            rate = uvt_wh.rate
            print('rate---->', rate)
            add_sum = UVT_SUM_FORMULA[rate] if UVT_SUM_FORMULA.get(rate) else 0
            print('uvt_wh.start_range---->', uvt_wh.start_range)
            print('add_sum---->', add_sum)
            value = (payment2uvt - uvt_wh.start_range) * Decimal(rate / 100) + add_sum
            print('value uvt----->', value)
            # res = round(value, 1) * uvt_config
            res = value * uvt_config
            print('respuesta >', res)
            res = Decimal(round(res, -3))
        return res
