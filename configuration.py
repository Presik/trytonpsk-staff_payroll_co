# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Id, Eval


class Configuration(metaclass=PoolMeta):
    __name__ = "staff.configuration"

    allow_zero_quantities = fields.Boolean('Allow Zero Quantities',
        help='If this checked the lines in payroll in zero do not delete')
    staff_liquidation_sequence = fields.Many2One('ir.sequence',
        'Liquidation Sequence', required=True, domain=[
            ('sequence_type', '=',
                Id('staff_payroll', 'sequence_type_payroll'))]
        )
    extras_limit = fields.Float('Extras Limit', help='In hours')
    uvt_value = fields.Numeric('UVT Value')
    liquidation_account = fields.Many2One('account.account',
        'Liquidation Account', domain=[
            ('type.payable', '=', True),
        ], required=True)
    payment_partial_sunday = fields.Boolean('Payment Partial Sunday')
    template_email_confirm = fields.Many2One('email.template', 'Template Email of Confirmation')
    limit_shift_month = fields.Integer('Limit Shift Month')
    wage_shift_fixed = fields.Many2One('staff.wage_type',
        'Wage Shift Fixed')
    tax_withholding = fields.Many2One('account.tax', 'Tax Withholding',
        domain=[('company', '=', Eval('company', -1))],
        depends=['company'])
